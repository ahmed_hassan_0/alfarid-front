/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');



// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('app-add-artists-to-event', require('./components/AddArtistToEventComponent.vue').default);
Vue.component('app-loading-gif', require('./components/LoadingComponent.vue').default);

//theme components
Vue.component('app-common-questions', require('./components/theme/CommonQuestionComponent.vue').default);
Vue.component('app-new-item', require('./components/theme/NewItemComponents.vue').default);

const Vuetify  = require('vuetify');

Vue.use(Vuetify);

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify({rtl: true})
});
