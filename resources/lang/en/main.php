<?php
/**
 * Created by PhpStorm.
 * User: Ahmed
 */

return [
    'arabic'        => 'Arabic',
    'english'        => 'English',
    'ar'        => 'عربي',
    'en'        => 'English',
    'theme-settings' => 'Theme Settings',
    'icon-theme'     => 'Website Icon',
    'icon-theme-ar'     => 'Website Icon Arabic',
    'icon-theme-en'     => 'Website Icon English',
    'color'          => 'Website Color',
    'font-color'     => 'Color Font',
    'paginate_count' => 'Paginate Count',
    'buttons'        => [
        'save'   => 'Save',
        'submit'    => 'Submit'
    ],
    'website-data'   => 'Website Info',
    'website-name'   => 'Website Name',
    'website-keywords' => 'Keywords',
    'website-desc'    => 'Description',
    'contact-us'    => 'Contact Us',
    'email'    => 'Email',
    'phone'    => 'Phone',
    'address'    => 'Address',
    'facebook'    => 'Facebook',
    'twitter'    => 'Twitter',
    'telegram'    => 'Telegram',
    'youtube'    => 'Youtube',
    'snapchat'    => 'Snapchat',
    'instagram'    => 'Instagram',
    'admin-theme-settings' => 'Admin Theme Settings',
    'icon-dashboard' => 'Dashboard Icon',
    'image-dashboard' => 'Dashboard Image',
    'dashboard-fond-color' => 'Dashboard Font Color',
    'dashboard-info' => 'Dashboard Info',
    'dashboard-title' => 'Dashboard Title',
    'dashboard-desc' => 'Dashboard Description',
    'add_new_field'  => 'New Feature',
    'default_language' => 'Default Language',
    'languages'     => 'Languages',
    'home-title'    => 'Home',
    'usefull-links'    => 'Usefull Links',
    'our_pages'    => 'Our Pages',
    'contact_us'    => 'Contact Us',
    'home'      => 'Home',
    'products'      => 'Products',
    'jobs'      => 'Jobs',
    'our_products'      => 'Our Products',
    'our_categories'      => 'Our Categories',
    'recent_products'      => 'Recent Products',
    'welcome_to'      => 'Welcome To',
    'read_more'      => 'Read More',
    'service_provide'      => 'Service we provide',
    'all'      => 'All',
    'more_view'      => 'More View',
    'client_review'      => 'Client review',
    'copyright'      => 'Copyright',
    'all_right_reserved'      => 'All Rights Reserved',
    'we_will' => 'We Will',
    'contact' => 'Contact',
    'successful_contact_us' => 'Thanks For Contact Us',
    'all_categories' => 'All Categories',
    'get_in_touch'    => '<span>Get</span> in touch',
    'mail_us'    => 'Mail Us',
    'call_us'    => 'Call Us',
    'features'    => 'Features',
    'populer_questions'    => '<span class="light-text">Populer</span> questions',
    'your_name'    => 'Your Name',
    'subject'    => 'Subject',
    'message'    => 'Message',
    'send_message'    => 'Send Message',
    'visit'    => 'Visit our office',
    'success_job' => 'this message has been sent',
    'banners'   => 'Banners',
    'products-banner'   => 'Products Page',
    'jobs-banner'   => 'Jobs Page',
    'about-us-banner'   => 'About us Page',
    'contact-us-banner'   => 'Contact us Page',
    'pages-banner'   => 'Pages',
    'job_application' => 'Job Application',
    'product_price_show' => 'Offer price',
    'name'    => 'Name',
    'company_name'    => 'Company Name',
    'quantity'    => 'Quantity',
    'successful_offer'    => 'Successful Offer',
    'visit'    => 'Visit',

    'job'    => 'job',
    'phone'    => 'phone',
    'cv'    => 'cv',
    'infromation'    => 'infromation',
    'send'    => 'send',
    'menu'    => 'Menu',
    'video-home'  => 'About Us Video',
    'gallary'  => 'gallary'
    
];
