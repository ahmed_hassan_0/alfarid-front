<div class="form-group">
    <label class="control-label" for="{{$nameInput}}">{{ $label }}</label>
    <select class="form-control select2-taggable" name="{{$nameInput}}[]" multiple="" {{ isset($required) ? 'required' : '' }}>
        @foreach($options as $option)
            <option {{ isset($selected) && in_array($option->id, $selected) ? 'selected' : '' }} value="{{ $option->id }}">{{ $option->name }}</option>
        @endforeach
    </select>
</div>
