@extends('theme.partials.master')

@section('content')
    <!--breadcumb start here-->
    <section class="banner-inner-sec" style="background-image:url('{{ asset('storage/' . _setting('pages-banner')) }}')">
        <div class="banner-table">
            <div class="banner-table-cell">
                <div class="container">
                    <div class="banner-inner-content">
                        <h2 class="banner-inner-title">{{ $page->getTranslatedAttribute('title', $locale) }}</h2>
                        <ul class="xs-breadcumb">
                            <li><a href="{{ url('') }}"> {{ __('main.home') }}  / </a>  {{ $page->getTranslatedAttribute('title', $locale) }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--breadcumb end here-->
    <!--  get in touch section -->
    <section class="xs-get-in-touch">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mx-auto">
                    <div class="get-in-touch-cont text-center">
                        {!! $page->getTranslatedAttribute('body', $locale) !!}
                    </div>
                </div>
            </div><!-- .row end -->
        </div><!-- .container end -->
    </section><!-- End get in touch section -->

    <!--  contact section -->
    <section class="xs-contact-infomation xs-contact-info-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-group">
                        <i class="icon-address"></i>
                        <h4>{{ __('main.visit') }}</h4>
                        <span>{{ _setting('address') }}</span>
                    </div><!-- .contact-info-group END -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-group">
                        <i class="icon-mail"></i>
                        <h4>{{ __('main.mail_us') }}</h4>
                        <a href="mailto:{{ _setting('email') }}">{{ _setting('email') }}</a>
                    </div><!-- .contact-info-group END -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-group">
                        <i class="icon-call"></i>
                        <h4>{{ __('main.call_us') }}</h4>
                        <span>{{ _setting('phone') }}</span>
                    </div><!-- .contact-info-group END -->
                </div>
            </div>
        </div><!-- .container end -->
    </section>
    <!-- End contact section -->
@endsection