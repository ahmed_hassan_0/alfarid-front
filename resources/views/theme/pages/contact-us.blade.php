@extends('theme.partials.master')

@section('content')
    <!--breadcumb start here-->
    <section class="banner-inner-sec" style="background-image:url('{{ asset('storage/' . _setting('contact-us-banner')) }}')">
        <div class="banner-table">
            <div class="banner-table-cell">
                <div class="container">
                    <div class="banner-inner-content">
                        <h2 class="banner-inner-title">{{ $page->getTranslatedAttribute('title', $locale) }}</h2>
                        <ul class="xs-breadcumb">
                            <li><a href="{{ url('') }}"> {{ __('main.home') }}  / </a>  {{ $page->getTranslatedAttribute('title', $locale) }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--breadcumb end here-->
    <!--  get in touch section -->
    <section class="xs-get-in-touch">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="get-in-touch-cont text-center">
                        {!! $page->getTranslatedAttribute('body', $locale) !!}
                    </div>
                </div>
            </div><!-- .row end -->
        </div><!-- .container end -->
    </section><!-- End get in touch section -->

    <!--  contact section -->
    <section class="xs-contact-sec">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="xs-form-group">
                        @if (session()->has('type'))
                            <div class="alert alert-{{ session()->get('type') }}">{{ session()->get('msg') }}</div>
                        @endif
                        <form method="post" action="{{ route('contact-us.submit') }}" class="xs-form">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <input required type="text" class="form-control" name="name" placeholder="{{ __('main.your_name') }}"
                                           id="xs_contact_name">
                                </div>
                                <div class="col-lg-6">
                                    <input required type="email" class="form-control" name="email" placeholder="{{ __('main.email') }} "
                                           id="xs_contact_email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input required type="number" class="form-control" name="phone"
                                           placeholder="{{ __('main.phone') }}" id="xs_contact_phone">

                                </div>
                                <div class="col-lg-6">
                                    <input required type="text" class="form-control" name="subject" placeholder="{{ __('main.subject') }}"
                                           id="xs_contact_subject">
                                </div>
                            </div>

                            <textarea required name="message" placeholder="{{ __('main.message') }}" id="x_contact_massage"
                                      class="form-control message-box" cols="30" rows="10"></textarea>

                            <div class="xs-btn-wraper">
                                <input type="submit" class="xs-btn" id="xs_contact_submit" value="{{ strtoupper(__('main.send_message')) }}">
                            </div>
                        </form>
                    </div>
                </div><!-- col end -->
                <div class="col-lg-5">
                    <div class="contact-map">
                        <div style="width: 100%">

                            <iframe src="https://maps.google.com/maps?width=100&amp;height=600&amp;hl=en&amp;q=Riyadh&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"></iframe>

                        </div>
                    </div>
                </div>
            </div><!-- .row end -->
        </div><!-- .container end -->
    </section><!-- End contact section -->

    <!--  contact section -->
    <section class="xs-contact-infomation xs-contact-info-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-group">
                        <i class="icon-address"></i>
                        <h4>{{ __('main.visit') }}</h4>
                        <span>{{ _setting('address') }}</span>
                    </div><!-- .contact-info-group END -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-group">
                        <i class="icon-mail"></i>
                        <h4>{{ __('main.mail_us') }}</h4>
                        <a href="mailto:{{ _setting('email') }}">{{ _setting('email') }}</a>
                    </div><!-- .contact-info-group END -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-group">
                        <i class="icon-call"></i>
                        <h4>{{ __('main.call_us') }}</h4>
                        <span>{{ _setting('phone') }}</span>
                    </div><!-- .contact-info-group END -->
                </div>
            </div>
        </div><!-- .container end -->
    </section>
    <!-- End contact section -->
@endsection