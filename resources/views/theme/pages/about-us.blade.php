
@extends('theme.partials.master')

@section('content')

    <!--breadcumb start here-->
    <section class="banner-inner-sec" style="background-image:url('{{ asset('storage/' . _setting('about-us-banner')) }}">
        <div class="banner-table">
            <div class="banner-table-cell">
                <div class="container">
                    <div class="banner-inner-content">
                        <h2 class="banner-inner-title">{{$page->getTranslatedAttribute('title', $locale)}}</h2>
                        <ul class="xs-breadcumb">
                            <li><a href="index.html"> {{ __('main.home') }}  / </a> {{ $page->getTranslatedAttribute('title', $locale) }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--breadcumb end here-->

    <!-- header about inner section -->
    <section class="about-inner section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="300ms">
                    <div class="about-inner-img">
                        <img src="{{ asset('theme/images/about/about_2.jpg') }}" alt="">
                    </div>
                </div><!-- col end -->
                <div class="col-lg-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                    <div class="about-inner-content">
                        <h2 class="column-title2 column-title">{{$page->getTranslatedAttribute('title', $locale)}}</h2>
                        <div class="single-about-content">
                            <h3>You will morning man open green fruitful</h3>
                            {!!$page->getTranslatedAttribute('body', $locale) !!}
                        </div>
                        
                        
                    </div>
                </div><!-- col end -->
            </div><!-- row end-->
        </div><!-- .container end -->
    </section>
    <!-- End about inner section -->

@endsection