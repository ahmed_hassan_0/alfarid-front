@extends('theme.partials.master')

@section('content')

<section class="gallery-sec recent-work-sec our-project-sec section-padding">
    <div class="container">
        
                    
                    
                    @foreach($query as $object)
                    <div class="single-project-content">
                        <div class="xs-image-popup-icon">
                            
                        </div>
                        <p>
                        <a href = "{{asset('storage/' .$object->image)}}">
                                <img width="200" src="{{asset('storage/' .$object->image)}}" alt="ssssssssssssssssssssssss">
                                </a>                        </p>
                    </div>
                </a><!-- .xs-single-portfolio-item END -->
                

        </div>
        @endforeach
        <div class="row">
            <div class="col-lg-12">
                <div class="load-more-btn">
                    <links>{{$query}}</links>
                </div>
            </div>
        </div><!-- row end-->
    </div><!-- .container end -->
</section>

@endsection