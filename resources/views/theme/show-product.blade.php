@extends('theme.partials.master')



@section('content')







    <!--breadcumb start here-->

    <section class="banner-inner-sec" style="background-image:url('{{ asset('storage/' . _setting('products-banner')) }}')">

        <div class="banner-table">

            <div class="banner-table-cell">

                <div class="container">

                    <div class="banner-inner-content">

                        <h2 class="banner-inner-title">{{ $product->getTranslatedAttribute('name', $locale) }}</h2>

                        <ul class="xs-breadcumb">

                            <li><a href="{{ url('') }}"> {{ __('main.home') }}  / </a> {{ __('main.products') }}</li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--breadcumb end here-->



    <!--  service inner section -->

    <section class="service-inner-sec single-service-sec section-padding">

        <div class="container">

            <div class="row">

                <div class="col-lg-3 col-md-4">

                    <div class="service-sidebar">



                        @include('theme.components.categories-widget', ['categories', $categories])



                    </div><!-- srvice sidebar -->

                </div><!-- col end -->

                <div class="col-lg-9 col-md-8">

                    <div class="main-single-service-content">

                        <div class="single-service-post-content">

                            <h2 class="column-title column-title2">{{ $product->getTranslatedAttribute('name', $locale) }}</h2>

                                @foreach(json_decode($product->image) as $image)
                            <img src="{{ asset('storage/' . $image) }}" alt="">

                            @endforeach

                            <p>{!! $product->getTranslatedAttribute('description', $locale) !!}</p>

                        </div>



                        <div class="row key-benifits-item xs-mb-60">

                            <div class="col-lg-12">

                                <div class="key-benifits-content">

                                    @php $features = _arrayGet(json_decode($product->features, true), $locale) @endphp

                                    @if(array_filter($features))

                                        <h3 class="xs-post-title">{{ __('main.features') }}</h3>

                                        <ul>

                                            @foreach($features as $feature)

                                                <li><i class="fa fa-caret-right"></i>{{ $feature }}</li>

                                            @endforeach

                                        </ul>

                                    @endif

                                </div>

                            </div>

                        </div>



                        <div class="row" style="margin-bottom: 40px">

                            <div class="col-lg-12">

                                <h2 class="xs-title" style="font-size: 22px">{{ __('main.product_price_show') }}</h2>

                                <div class="xs-form-group">

                                    @if (session()->has('type'))

                                        <div class="alert alert-{{ session()->get('type') }}">{{ session()->get('msg') }}</div>

                                    @endif

                                    <form method="post" action="{{ route('product-price.submit') }}" class="xs-form">

                                        @csrf

                                        <input hidden name="product_id" value="{{ $product->id }}">

                                        <input hidden name="localee" value="{{ $locale }}">

                                        <div class="row">

                                            <div class="col-lg-12">

                                                <input required type="text" class="form-control" name="name" placeholder="{{ __('main.name') }}"

                                                       id="xs_contact_name">

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-lg-12">

                                                <input required type="text" class="form-control" name="company_name" placeholder="{{ __('main.company_name') }}"

                                                       id="xs_contact_name">

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-lg-12">

                                                <input required type="text" class="form-control" name="quantity" placeholder="{{ __('main.quantity') }}"

                                                       id="xs_contact_name">

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-lg-12">

                                                <input type="email" class="form-control" name="email" placeholder="{{ __('main.email') }}"

                                                       id="xs_contact_name">

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-lg-12">

                                                <input type="text" class="form-control" name="phone" placeholder="{{ __('main.phone') }}"

                                                       id="xs_contact_name">

                                            </div>

                                        </div>

                                        <div class="xs-btn-wraper">

                                            <input type="submit" class="xs-btn" id="xs_contact_submit" value="{{ strtoupper(__('main.send_message')) }}">

                                        </div>

                                    </form>

                                </div>

                            </div><!-- col end-->

                        </div>



                        <div class="row populer-question-item">

                            <div class="col-lg-12">

                                @include('theme.components.FAQ-widget')

                            </div><!-- col end-->

                        </div>

                    </div>

                </div><!-- col end-->



            </div><!-- row end-->

        </div><!-- .container end -->

    </section><!-- End service inner section -->





@endsection