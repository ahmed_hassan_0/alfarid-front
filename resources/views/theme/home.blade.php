@extends('theme.partials.master')

@section('pageTitle', __('main.home'))

@section('content')

    <!-- start banner section -->
    <section class="xs-banner-sec owl-carousel banner-slider">

        @foreach ($sliders as $slider)

            <div class="banner-slider-item banner-item1" style="background-image: url({{ asset('storage/' . str_replace('\\', '/', $slider->image)) }});">
                <div class="slider-table">
                    <div class="slider-table-cell">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="banner-content text-center">
                                        <h2>{{ $slider->getTranslatedAttribute('title', app()->getLocale()) }}</h2>
                                        <p>{!! $slider->getTranslatedAttribute('text', app()->getLocale()) !!}</p>
                                        <div class="xs-btn-wraper">
                                            <a href="{{ route('products.' . $locale, ['locale' => $locale]) }}" class="xs-btn">
                                                {{ __('main.our_products') }}
                                            </a>
                                        </div><!-- .xs-btn-wraper END -->
                                    </div><!-- .xs-welcome-wraper END -->
                                </div><!-- .column END -->
                            </div><!-- .row end -->
                        </div><!-- .container end -->
                    </div><!-- .slider table cell end -->
                </div><!-- .slider table end -->
            </div><!-- .xs-welcome-content END -->

        @endforeach

    </section><!-- End banner section -->

    <!-- start about us section -->
    <section class="about-sec section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-5 wow fadeInUp align-self-center" data-wow-duration="1.5s" data-wow-delay="300ms">
                    <div class="about-content">
                        @php $about_us = \App\Page::find(\App\Helpers\Pages::AboutUs) @endphp
                        <h2 class="column-title"> <span class="xs-title">{{ $about_us->getTranslatedAttribute('title', $locale) }}</span>{{ __('main.welcome_to') }} <br> {{ _setting('theme-name') }}!</h2>
                        <p>{{ \Illuminate\Support\Str::limit(strip_tags($about_us->getTranslatedAttribute('body', $locale)), 400) }}</p>
                        <a href="{{ url($locale. '/' . $about_us->getTranslatedAttribute('slug', $locale)) }}" class="xs-btn sm-btn">{{ __('main.read_more') }}</a>
                    </div>
                </div>
                <div class="col-md-7 align-self-center wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                    <div class="about-video-item">
                        <div class="about-video-img">
                            <img src="{{ asset('theme/images/about_front.jpg') }}" alt="">
                            <a href="{{ _setting('about-us-video') }}" class="xs-video"><i class="fa fa-play"></i></a>
                        </div>
                        <img class="about-img2" src="{{ asset('theme/images/about_back.jpg') }}" alt="">
                    </div>
                </div>
            </div><!-- row end-->
        </div><!-- .container end -->
    </section><!-- End about us section -->

    <!-- start service section -->
    <section class="service-sec section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-item">

                        <h2 class="section-title">
                            <span class="xs-title">{{ __('main.service_provide') }}</span>
                            {{ __('main.our_products') }}
                        </h2>
                    </div>
                </div>
            </div><!-- row end-->
            <div class="row">
                @php $recent_products = $products->take(4) @endphp
                @foreach ($recent_products as $recent_product)
                    <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="300ms">
                        <div class="single-service">
                            <div class="service-img">
                                <img style="height: 250px;" src="{{ asset('storage/' . $recent_product->image) }}" alt="">
                                <i class="icon-Our_service_1"></i>
                            </div>
                            <h3 class="xs-service-title"><a href="#">{{ $recent_product->getTranslatedAttribute('name', $locale) }}</a> </h3>
                            <p>{{ \Illuminate\Support\Str::limit(strip_tags($recent_product->getTranslatedAttribute('description', $locale))) }}</p>
                            <a href="{{ route('products.' . $locale, ['locale' => $locale]) }}" class="readMore">{{ __('main.read_more') }} <i class="icon icon-arrow-right"></i> </a>
                        </div>
                    </div>
                @endforeach
            </div><!-- row end-->
        </div><!-- .container end -->
    </section><!-- End service section -->

    <!-- start service section -->
    <section class="recent-work-sec section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-item">

                        <h2 class="section-title">
                            <span class="xs-title">{{ __('main.our_categories') }}</span>
                            {{ __('main.recent_products') }}
                        </h2>
                    </div>
                    <div class="recent-folio-menu">
                        <ul>
                            <li  class="active filter" data-filter="all">{{ __('main.all') }}</li>
                            @foreach ($categories as $category)
                                <li  class="filter" data-filter=".category-{{$category->id}}">{{ $category->getTranslatedAttribute('name', $locale) }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div><!-- row end-->
            <div class="row" id="mixcontent">
                @php $products_with_categories = $products; //$products->skip(4) @endphp
                @foreach ($products_with_categories as $product)
                    <div class="col-lg-4 mix category-{{$product->category_id}} col-sm-6">
                        <a href="{{ route('products.show.' . $locale, ['locale' => $locale, 'slug' => $product->getTranslatedAttribute('slug', $locale)]) }}">
                            <div class="single-recent-work">
                                <img src="{{ asset('storage/' . $product->image) }}" alt="">
                                <div class="recet-work-footer">
                                    <i class="icon-Our_service_3"></i>
                                    <h3>{{ $product->getTranslatedAttribute('name', $locale) }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div><!-- row end-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="load-more-btn">
                        <a href="{{ route('products.' . $locale , ['locale' => $locale]) }}" class="xs-btn fill">{{ __('main.more_view') }}</a>
                    </div>
                </div>
            </div><!-- row end-->
        </div><!-- .container end -->
    </section><!-- End service section -->

    <!-- start testmonila section -->
    <section class="testmonial-sec">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="300ms">
                    <div class="call-back-content">
                        <h3>{{ __('main.we_will') }} <span>{{ __('main.contact') }}</span></h3>
                        @if (session()->has('type'))
                            <div class="alert alert-{{ session()->get('type') }}">{{ session()->get('msg') }}</div>
                        @endif
                        <form class="call-back-form" method="post" action="{{ route('contact-us.submit') }}">
                            @csrf
                            <div class="form-group">
                                <input required type="text" name="name" value="" placeholder="{{ __('main.your_name') }}" class="call-back-inp">
                            </div>
                            <div class="form-group">
                                <input required type="email" name="email" value="" placeholder="{{ __('main.email') }} " class="call-back-inp">
                            </div>
                            <div class="form-group">
                                <input required type="number" name="phone" value="" placeholder="{{ __('main.phone') }}" class="call-back-inp">
                            </div>
                            <div class="form-group">
                                <input required type="text" name="subject" value="" placeholder="{{ __('main.subject') }}" class="call-back-inp">
                            </div>
                            <div class="form-group xs-mb-40">
                                <textarea required name="message" placeholder="{{ __('main.message') }}" class="call-back-inp call-back-msg"></textarea>
                            </div>
                            <div class="form-group ">
                                <button type="submit" name="submit" class="xs-btn">{{ __('main.buttons.submit') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                    <div class="owl-carousel" id="testmonial-slider">
                        @foreach ($testimonials as $testimonial)
                            <div class="testmonial-content ">
                                <i class="testmonial_icon icon-client_review"></i>
                                <h3 class="testmonial-title">{{ __('main.client_review') }}</h3>
                                <p>{!! $testimonial->getTranslatedAttribute('testimonial', $locale) !!}</p>
                                <div class="testmonial-author">
                                    <img src="{{ asset('storage/' . $testimonial->image) }}" alt="">
                                    <h4>{{ $testimonial->name }}</h4 >
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div><!-- row end-->
        </div><!-- .container end -->
    </section><!-- End testmonila section -->

    <!-- start map section -->
    <div class="xs-map-sec">
        <div class="xs-maps-wraper">
            <div class="map">
                <iframe src="https://maps.google.com/maps?width=100&amp;height=600&amp;hl=en&amp;q=Riyadh&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"></iframe>
            </div>
        </div>
    </div><!-- End map section -->

@endsection