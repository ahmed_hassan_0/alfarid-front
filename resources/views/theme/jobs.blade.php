@extends('theme.partials.master')

@section('content')

    <!--breadcumb start here-->
    <section class="banner-inner-sec" style="background-image:url('{{ asset('storage/' . _setting('jobs-banner')) }}')">
        <div class="banner-table">
            <div class="banner-table-cell">
                <div class="container">
                    <div class="banner-inner-content">
                        <h2 class="banner-inner-title">{{ __('main.jobs') }}</h2>
                        <ul class="xs-breadcumb">
                            <li><a href="/"> {{ __('main.home') }}  / </a> {{ __('main.jobs') }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--breadcumb end here-->
    <!--  service inner section -->
    <section class="service-v2-sec service-inner-sec section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3">
                    
                </div><!-- col end -->
                <div class="col-lg-9 col-md-8">
                    <div class="row">
                    @foreach($items as $item)
                        <div class="col-lg-4 col-md-6">
                            <div class="single-services-item">
                                
                                <h3 class="xs-service-title"><a href="jobs/{{$item->id}}">{{$item->getTranslatedAttribute('title', $locale)}}</a></h3>
                                {!! $item->getTranslatedAttribute('description', $locale) !!}
                            </div>
                        </div>
                       @endforeach
                    </div><!-- row end-->
                    <links>{{$items}}</links>
                </div><!-- col end-->

            </div><!-- row end-->
        </div><!-- .container end -->
    </section><!-- End service inner section -->

@endsection