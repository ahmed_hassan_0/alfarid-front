<h3 class="xs-service-title">{!! __('main.populer_questions') !!}</h3>
<div class="faq-list-item" id="accordion">
    @php $common_questions = \App\CommonQuestion::orderBy('order')->get() @endphp
    @foreach($common_questions as $key => $common_question)
        <div class="faq-single-item">
            <div class="card-header" id="heading-{{ $key }}">
                <h4>

                    <button class="btn btn-link {{ !$loop->first ? 'collapsed' : '' }}" data-toggle="collapse" data-target="#collapse-q-{{ $key }}"
                            aria-expanded="true" aria-controls="collapse-q-{{ $key }}">
                        <span>{{ $key + 1 }}</span>
                        {{ $common_question->getTranslatedAttribute('question', $locale) }}
                    </button>
                </h4>
            </div>

            <div id="collapse-q-{{ $key }}" class="collapse {{ $loop->first ? 'show' : '' }}" aria-labelledby="heading-{{ $key }}"
                 data-parent="#accordion">
                <div class="card-body">
                    <p>{!! $common_question->getTranslatedAttribute('answer', $locale) !!}</p>
                </div>
            </div>
        </div>
    @endforeach
</div>