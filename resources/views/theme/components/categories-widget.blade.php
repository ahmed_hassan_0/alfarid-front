<div class="widgets">
    <h3 class="widget-title">{{ __('main.our_categories') }}</h3>
    <ul class="services-link-item">
        <li class="{{ request()->category ? '' : 'active' }}"><a href="{{ route('products.' . $locale, ['locale' => $locale]) }}">{{ __('main.all_categories') }}</a></li>
        @foreach($categories as $category)
            <li class="{{ $category->getTranslatedAttribute('slug') == request()->category ? 'active' : '' }}"><a href="{{ route('products.' . $locale, ['locale' => $locale,'category' => $category->getTranslatedAttribute('slug')]) }}">{{ $category->getTranslatedAttribute('name', $locale) }}</a></li>
        @endforeach
    </ul>
</div><!-- widgets -->