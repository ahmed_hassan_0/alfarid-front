<!-- header nav section -->

<header class="xs-header-nav">

    <div class="container">

        <div class="row  menu-item">

            <div class="col-lg-12">

                <nav id="navigation1" class="navigation header-nav clearfix">



                    <div class="nav-header">

                        <!--  <a class="nav-brand" href="#"></a>-->

                        {{--<a href="./index.html" class="mobile-logo">--}}

                            {{--<img width="30px" src="{{ asset('storage/' . _setting('theme-icon_image')) }}" alt="">--}}

                        {{--</a>--}}

                        <div class="nav-toggle"><span class="menu-nav" style="position: relative;right: 45px;color: #fff;top: -4px;">{{ __('main.menu') }}</span></div>

                    </div>



                    <div class="nav-menus-wrapper clearfix">

                        <ul class="nav-menu">

                            <li class="active"><a href="{{ route('home', ['locale' => $locale]) }}">{{ __('main.home') }}</a></li>

                            <li><a href="{{ route('products.' . $locale, ['locale' => $locale]) }}">{{ __('main.products') }}</a></li>

                            <li><a href="{{ route('jobs.' . $locale, ['locale' => $locale]) }}">{{ __('main.jobs') }}</a></li>
                            
                            <li><a href="{{ route('gallary.' . $locale, ['locale' => $locale]) }}">{{ __('main.gallary') }}</a></li>

                            @foreach($dynamicPages as $dynamicPage)

                                <li><a href="{{ url($locale . '/' . $dynamicPage->getTranslatedAttribute('slug', app()->getLocale())) }}">{{ $dynamicPage->getTranslatedAttribute('title', app()->getLocale()) }}</a></li>

                            @endforeach

                        </ul>



                        <div class="header-nav-right-info align-to-right">

                            <label><i class="icon icon-phone3"></i>{{ _setting('phone') }}</label>

                        </div>



                    </div>



                </nav>

            </div>

        </div><!-- .row end -->

    </div><!-- .container end -->

</header><!-- End header nav section -->