<!-- get_header('Page Name','Title')-->
<!doctype html>
<html class="no-js" lang="{{ $locale }}">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ _setting('theme-name') }} - @if(!isset($pageTitle)) @yield('pageTitle') @else {{ $pageTitle }} @endif</title>
    <meta name="description" content="{{ _setting('theme-description') }}">
    <meta name="keywords" content="{{ _setting('theme-keywords') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <link rel="shortcut icon" type="image/png" href="{{ asset('storage/' . _setting('theme-icon_image')) }}">
    <!-- Place favicon.ico in the root directory -->
    {{--<link rel="apple-touch-icon" href="apple-touch-icon.png">--}}

    <link rel="stylesheet" href="{{ asset('theme/css/fontawesome-min.css') }}">

    @if($locale == 'en')
        <link rel="stylesheet" href="{{ asset('theme/css/bootstrap.min.css') }}">
    @else
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css" integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('theme/css/main-rtl.css') }}">
    @endif

    <link rel="stylesheet" href="{{ asset('theme/css/xsIcon.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/iconmoon.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/isotope.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/navigation.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/animate.css') }}">

    <!--Theme custom css -->
    <link rel="stylesheet" href="{{ asset('theme/css/style.css') }}">

    <!--Theme Responsive css-->
    <link rel="stylesheet" href="{{ asset('theme/css/responsive.css') }}"/>

    <!-- use only color version -->
    <!-- <link rel='stylesheet' type='text/css' href='assets/css/colors/color-1.css' > -->

    <style>
        @php $main_bg_color = _setting('theme-color', '#008435') @endphp
        @php $main_color = _setting('theme-font-color', '#008435') @endphp
        @php $main_bg_color2 = rgba($main_bg_color, .9) @endphp

        {{-- Background Color --}}
        .main-bg-color,
        .xs-header-top,
        .promo-area-sec .promo-content-item .single-promo-content i,
        .service-sec .single-service .service-img i,
        .service-sec .single-service:hover .service-img i,
        .recent-work-sec .single-recent-work .recet-work-footer i,
        #preloader,
        .recent-work-sec .recent-folio-menu ul li.active:before,
        .pricing-plan-sec .pricing-plan-item .single-pricing-plan .pricing-bar,
        .xs-header-nav .header-nav .nav-menu > li > a:hover,
        .service-inner-sec .service-sidebar .widgets .services-link-item li.active a,
        .service-inner-sec .service-sidebar .widgets .services-link-item li:hover a,
        .service-inner-sec .service-sidebar .widgets .services-link-item li.active a,
        .service-inner-sec .service-sidebar .widgets .services-link-item li:hover a,
        .service-inner-sec .service-sidebar .widgets .brochures-list li a i,
        .testmonial-sec #testmonial-slider .owl-dots .owl-dot.active
        {
            background-color: {{ $main_bg_color }} !important;
        }

        /* Main Color */
        .main-color,
        .xs-title,
        .recent-work-sec .recent-folio-menu ul li.active,
        .why-choose-us-sec .why-choose-content .single-why-choose-list h3 i,
        .pricing-plan-sec .pricing-plan-item .single-pricing-plan h3,
        .testmonial-sec .testmonial-content .testmonial-author .author-rating i,
        .column-title.white span,
        .cancel-preloader,
        pricing-plan-sec .pricing-plan-item .single-pricing-plan i,
        .banner-inner-sec .banner-inner-content .xs-breadcumb li a,
        .service-inner-sec .service-sidebar .widgets .widget-title,
        .service-inner-sec .service-sidebar .widgets .contact-list li label,
        .service-inner-sec .service-sidebar .widgets .contact-list li i,
        .xs-footer-sec .footer-top-item .xs-back-to-top a,
        .xs-footer-sec .copyright-content p a,
        .about-inner .about-inner-content .single-about-funfact i,
        .about-inner .about-inner-content .single-about-funfact h4,
        .pricing-plan-sec .pricing-plan-item .single-pricing-plan i
        .xs-header-top .header-top-social ul li a:hover,
        .testmonial-sec .testmonial-content i.testmonial_icon,
        .testmonial-sec .call-back-content h3 span
        {
            color: {{ $main_color }}  !important;
        }
        .xs-header-nav .header-nav {
            background-color: {{ $main_bg_color2 }};
        }
        .xs-btn {
            background-color: {{ $main_bg_color2 }};
            border: 1px solid {{ $main_bg_color2 }};
        }
        .recent-work-sec .single-recent-work .recet-work-footer h3 {
            background: {{ rgba($main_bg_color, 0.9) }};
        }
        .load-more-btn .fill {
            border-color: {{ $main_color }};
            color: {{ $main_color }};
        }
        .why-choose-us-sec .why-choose-img:after {
            border-right: 20px solid {{ $main_bg_color }};
            border-bottom: 20px solid {{ $main_bg_color }};
        }
        .xs-header-nav .header-nav .nav-menu > li > a {
            border-left: 1px solid {{ $main_bg_color2 }};
            border-right: 1px solid {{ $main_bg_color2 }};
        }
        .xs-header-nav .header-nav:after {
            border-color: transparent transparent transparent {{ $main_bg_color }};
        }
        .xs-btn:hover, .xs-btn:focus {
            outline: none;
            background: {{ $main_bg_color }};
            border-color: {{ $main_bg_color }};
        }
        .promo-area-sec .promo-content-item .single-promo-content i {
            color: #FFFFFF;
        }

    </style>

    @if($locale == 'ar')
        <link rel="stylesheet" href="{{ asset('theme/css/main-rtl.css') }}">
    @endif

    @stack('css')

    </head>
    <body>
        <!--[if lt IE 10]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
            your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="preloader">
            <div class="preloader-wrapper">
                <div class="la-ball-clip-rotate-pulse la-2x">
                    <div></div>
                    <div></div>
                </div>
            </div>
            <a href="" class="cancel-preloader">Cancel Preloader</a>
        </div>	<!-- END prelaoder -->

        <!-- header top section -->
        <section class="xs-header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-8">
                    <div class="header-top-info">
                        <ul>
                            <li><i class="icon icon-map-marker2"></i>{{ _setting('address') }}</li>
                            <li><i class="icon icon-envelope"></i>{{ _setting('email') }}</li>
                        </ul>
                    </div>
                </div><!-- .col end -->

                <div class="col-lg-6 align-self-center col-md-4">
                    <div class="header-top-social">
                        <ul>
                            @if (_setting('facebook'))<li><a target="_blank" href="//{{ _setting('facebook') }}"><i class="fa fa-facebook-f"></i></a></li>@endif
                            @if (_setting('twitter'))<li><a target="_blank" href="//{{ _setting('twitter') }}"><i class="fa fa-twitter"></i></a></li>@endif
                            @if (_setting('telegram'))<li><a target="_blank" href="//{{ _setting('telegram') }}"><i class="fa fa-telegram"></i></a></li>@endif
                            @if (_setting('instagram'))<li><a target="_blank" href="//{{ _setting('instagram') }}"><i class="fa fa-instagram"></i></a></li>@endif
                            @if (_setting('youtube'))<li><a target="_blank" href="//{{ _setting('youtube') }}"><i class="fa fa-youtube"></i></a></li>@endif
                            @if (_setting('snapchat'))<li><a target="_blank" href="//{{ _setting('snapchat') }}"><i class="fa fa-snapchat"></i></a></li>@endif
                            <li><a href="{{ url($locale == 'ar' ? 'en' : 'ar') }}">{{ __('main.' . ($locale == 'ar' ? 'en' : 'ar')) }}</a></li>
                        </ul>
                    </div>
                </div><!-- .col end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </section><!-- End header section -->

        <!-- header middle section -->
        <section class="xs-header-middle">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 align-self-center">
                        {{--<div class="logo">--}}
                            {{--<a href="./index.html">--}}
                                {{--<img src="assets/images/logo.png" alt="">--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    </div>
                    <div class="align-self-center col-md-9">
                        <div class="header-middle-info float-right">
                            <ul>
                                <li>
                                    <img width="150px" src="{{ asset('storage/' . _setting('theme-icon_image')) }}">
                                    {{--<label>Best Gardening</label>--}}
                                    {{--<p>Service provider of 2017</p>--}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- .container end -->
        </section><!-- End header middle section -->

        <!-- header nav section -->
        @include('theme.partials.nav')

        {{-- Content --}}
        @yield('content')

        @include('theme.partials.footer')

        @include('theme.partials.scripts')

        @stack('js')

    </body>
<!-- footer section end -->
</html>