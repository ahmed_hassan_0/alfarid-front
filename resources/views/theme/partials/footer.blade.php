<!-- footer section start -->
<footer class="xs-footer-sec">
    <div class="container">
        <div class="footer-top-item">
            <div class="xs-back-to-top">
                <a href="#" class="BackTo"><i class="fa fa-angle-double-up"></i> </a>
            </div>

        </div>

        <div class="footer-item">
            <div class="row">
                <div class="col-lg-3">
                    <div class="widgets">
                        <div class="footer-logo">
                            <a href="{{ url('') }}"><img width="150px" src="{{ asset('storage/' . _setting('theme-icon_image')) }}"></a>
                        </div>
                        <p>
                            {{ _setting('theme-description') }}
                        </p>
                        <ul class="footer-social">
                            @if (_setting('facebook'))<li><a target="_blank" href="//{{ _setting('facebook') }}"><i class="fa fa-facebook-f"></i></a></li>@endif
                            @if (_setting('twitter'))<li><a target="_blank" href="//{{ _setting('twitter') }}"><i class="fa fa-twitter"></i></a></li>@endif
                            @if (_setting('telegram'))<li><a target="_blank" href="//{{ _setting('telegram') }}"><i class="fa fa-telegram"></i></a></li>@endif
                            @if (_setting('instagram'))<li><a target="_blank" href="//{{ _setting('instagram') }}"><i class="fa fa-instagram"></i></a></li>@endif
                            @if (_setting('youtube'))<li><a target="_blank" href="//{{ _setting('youtube') }}"><i class="fa fa-youtube"></i></a></li>@endif
                            @if (_setting('snapchat'))<li><a target="_blank" href="//{{ _setting('snapchat') }}"><i class="fa fa-snapchat"></i></a></li>@endif
                        </ul>
                    </div>
                </div><!-- col end -->
                <div class="col-md-3">
                    <div class="widgets">
                        <h3 class="widget-title">{{ __('main.usefull-links') }}</h3>
                        <ul>
                            <li><a href="{{ url($locale) }}">{{ __('main.home') }}</a></li>
                            <li><a href="{{ route('products.' . $locale, ['locale' => $locale]) }}">{{ __('main.products') }}</a></li>
                            <li><a href="{{ route('jobs.' . $locale, ['locale' => $locale]) }}">{{ __('main.jobs') }}</a></li>
                            @foreach($dynamicPages as $dynamicPage)
                                <li><a href="{{ url($locale . '/' . $dynamicPage->getTranslatedAttribute('slug', app()->getLocale())) }}">{{ $dynamicPage->getTranslatedAttribute('title', app()->getLocale()) }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widgets">
                        <h3 class="widget-title">{{ __('main.our_pages') }}</h3>
                        <ul class="service-link">
                            @php $pages = \App\Page::active()->whereNotIn('id', [\App\Helpers\Pages::AboutUs, \App\Helpers\Pages::ContactUs])->with('translations')->get() @endphp
                            @foreach ($pages as $page)
                                <li>
                                    <a href="{{ url(app()->getLocale() . '/' . $page->getTranslatedAttribute('slug', app()->getLocale())) }}">{{ $page->getTranslatedAttribute('title', app()->getLocale()) }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widgets">
                        <h3 class="widget-title">{{ __('main.contact_us') }}</h3>
                        <ul class="footer-contact-list">
                            <li>
                                <i class="icon icon-map-marker2"></i>
                                {{ _setting('address') }}
                            </li>
                            <li>
                                <i class="icon icon-phone3"></i>
                                {{ _setting('phone') }}
                            </li>
                            <li>
                                <i class="icon icon-envelope"></i>
                                {{ _setting('email') }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- row end -->
        </div>
        <div class="copyright-content">
            <div class="row">
                <div class="col-md-6">
                    <p>{{ __('main.copyright') }} 2018, <a href="{{ url('') }}">{{ _setting('theme-name') }}</a>. {{ __('main.all_right_reserved') }}.</p>
                </div>
            </div>
        </div><!-- copyright end -->
    </div><!-- container end -->
</footer>