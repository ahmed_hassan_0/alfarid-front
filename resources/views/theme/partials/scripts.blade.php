<script src="{{ asset('theme/js/jquery-3.2.1.min.js') }}"></script>

@if($locale == 'en')
    <script src="{{ asset('theme/js/bootstrap.min.js') }}"></script>
@else
    <script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js" integrity="sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k" crossorigin="anonymous"></script>
@endif

<script src="{{ asset('theme/js/jquery-mixtub.js') }}"></script>
<script src="{{ asset('theme/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('theme/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('theme/js/navigation.js') }}"></script>
<script src="{{ asset('theme/js/jquery.appear.min.js') }}"></script>
<script src="{{ asset('theme/js/isotope.js') }}"></script>
<script src="{{ asset('theme/js/wow.min.js') }}"></script>
<script src="{{ asset('theme/js/main.js') }}"></script>