@extends('theme.partials.master')
@section('content')

    <section class="m-contact-sec" style="margin: 30px 0px;">
        <div class="container">
            <div class="row" style="margin-bottom:100px;">
                <div class="col-lg-12" >
                    
                    
                    
                    <div class="card" style="margin-top:30px;">
    
        

    <h5 class="card-header info-color white-text text-center py-4">
        <strong>{!! $item->getTranslatedAttribute('title', $locale) !!}</strong>
    </h5>

    <div class="card-body info-color white-text text-center py-4">

        {!! $item->getTranslatedAttribute('description', $locale) !!}

    </div>

</div>

                    <div class="xs-form-group">
    <h5 class="card-header info-color white-text text-center py-4">
        <strong>{{ __('main.job_application') }}</strong>
    </h5>
    
      @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
        @endif
                    <form style="margin-top:10px" method="post" enctype="multipart/form-data" action="/{{$locale}}/jobs" class="xs-form" style="margin-bottom:100px">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>{{ __('main.name') }}*</label>
                                    <input required type="text" class="form-control" name="name" placeholder="{{ __('main.name') }}"
                                           id="xs_contact_name">
                                </div>
                                <div class="col-lg-6">
                                    <label>{{ __('main.email') }}*</label>
                                    <input required type="email" class="form-control" name="email" placeholder="{{ __('main.email') }} "
                                           id="xs_contact_email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>{{ __('main.phone') }}*</label>
                                    <input required type="number" class="form-control" name="phone" min='0'
                                           placeholder="{{ __('main.phone') }}" id="xs_contact_phone">

                                </div>
                                <div class="col-lg-6">
                                    <label>{{ __('main.cv') }}*</label>
                                    <input type="file" required name="cv" class="form-control"
                                           >
                                </div>
                            </div>
                                <label>{{ __('main.cv') }}*</label>
                            <textarea required name="info" placeholder="{{ __('main.infromation') }}" id="x_contact_massage"
                                      class="form-control message-box" cols="30" rows="10"></textarea>
                        <input type="hidden"name="job_id" value="{{$item->id}}" id="materialSubscriptionFormPasswords" class="form-control" readonly >
                            <div class="xs-btn-wraper">
                                <input type="submit" class="xs-btn" id="xs_contact_submit" value="{{ strtoupper(__('main.send_message')) }}">
                            </div>
                        </form>
</div>

</div>
</div>
</div>
</div>
</section>
   
@endsection