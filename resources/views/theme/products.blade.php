@extends('theme.partials.master')



@section('content')



    <!--breadcumb start here-->

    <section class="banner-inner-sec" style="background-image:url('{{ asset('storage/' . _setting('products-banner')) }}')">

        <div class="banner-table">

            <div class="banner-table-cell">

                <div class="container">

                    <div class="banner-inner-content">

                        <h2 class="banner-inner-title">{{ __('main.products') }}</h2>

                        <ul class="xs-breadcumb">

                            <li><a href="{{ url('') }}"> {{ __('main.home') }}  / </a> @if(isset($category) && $category != null) <a href="{{ route('products.' . $locale, ['locale' => $locale]) }}"> {{ __('main.products') }}  / </a> @else {{ __('main.products') }} @endif @if(isset($category) && $category != null) {{ $category->getTranslatedAttribute('name', $locale) }} @endif</li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!--breadcumb end here-->



    <!--  service inner section -->

    <section class="service-v2-sec service-inner-sec section-padding">

        <div class="container">

            <div class="row">

                <div class="col-lg-3 col-md-4">

                    <div class="service-sidebar">



                        @include('theme.components.categories-widget', ['categories', $categories])



                        <div class="widgets">

                            <h3 class="widget-title">{!! __('main.get_in_touch') !!}</h3>

                            <ul class="contact-list">

                                <li><i class="icon icon-map-marker2"></i>{{ _setting('address') }}</li>

                                <li>

                                    <i class="icon icon-envelope4"></i>

                                    <label>  {{ __('main.mail_us') }}</label>

                                    {{ _setting('email') }}

                                </li>

                                <li>

                                    <i class="icon icon-phone3"></i>

                                    <label>  {{ __('main.call_us') }}</label>

                                    {{ _setting('phone') }}

                                </li>

                            </ul>

                        </div><!-- widgets -->

                    </div><!-- srvice sidebar -->

                </div><!-- col end -->

                <div class="col-lg-9 col-md-8">

                    <div class="row">

                        @foreach($products as $product)

                            <div class="col-lg-4 col-md-6">

                                <div class="single-services-item">

                                    <img src="{{ asset('storage/' . json_decode($product->image)[0]) }}" alt="">

                                    <h3 class="xs-service-title"><a href="{{ route('products.show.' . $locale, ['locale' => $locale, 'slug' => $product->getTranslatedAttribute('slug', $locale)]) }}">{{ $product->getTranslatedAttribute('name', $locale) }}</a></h3>

                                    <p>{{ \Illuminate\Support\Str::limit(strip_tags($product->getTranslatedAttribute('description', $locale)), 200) }}</p>

                                </div>

                            </div>

                        @endforeach

                    </div><!-- row end-->



                    @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator)

                        {!! $products->appends(['category' => request()->category])->links() !!}

                    @endif



                </div><!-- col end-->



            </div><!-- row end-->

        </div><!-- .container end -->

    </section><!-- End service inner section -->



@endsection