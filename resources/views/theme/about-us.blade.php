
@extends('theme.partials.master')

@section('content')

    <!--breadcumb start here-->
    <section class="banner-inner-sec" style="background-image:url('{{ asset('theme/images/banner/about-banner.jpg') }}')">
        <div class="banner-table">
            <div class="banner-table-cell">
                <div class="container">
                    <div class="banner-inner-content">
                        <h2 class="banner-inner-title">About Us</h2>
                        <ul class="xs-breadcumb">
                            <li><a href="index.html"> Home  / </a> About</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--breadcumb end here-->

    <!-- header about inner section -->
    <section class="about-inner section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="300ms">
                    <div class="about-inner-img">
                        <img src="{{ asset('theme/images/about/about_2.jpg') }}" alt="">
                    </div>
                </div><!-- col end -->
                <div class="col-lg-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                    <div class="about-inner-content">
                        <h2 class="column-title2 column-title">About our Company</h2>
                        <div class="single-about-content">
                            <h3>You will morning man open green fruitful</h3>
                            <p>And face of forth days for his lesser earth thing sea fourth. Brought. Fifth of form. Together.
                                Female whose said lights greater open gathered gathering.
                            </p>
                        </div>
                        <div class="single-about-content">
                            <h3>That had air creature us</h3>
                            <p>Multiply made after, herb man Won't a good open darkness
                                our deep heaven seasons they're day.
                            </p>
                        </div>
                        
                    </div>
                </div><!-- col end -->
            </div><!-- row end-->
        </div><!-- .container end -->
    </section>
    <!-- End about inner section -->

@endsection