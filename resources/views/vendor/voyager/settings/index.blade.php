@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.__('voyager::generic.settings'))

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-settings"></i> {{ __('voyager::generic.settings') }}
    </h1>
@stop

@section('content')
    <div class="container-fluid">
        @include('voyager::alerts')
    </div>

        <div class="page-content container-fluid">
            <div class="col-md-6 pull-right">
                <form action="{{ route('voyager.settings.store') }}?fields=admin-bg_image,admin-icon_image,admin-color,refresh" method="POST" class="ajax-form">
                <div class="panel panel-body">
                    <div class="loading" style="background-image: url({{ asset('dashboard/images/loader.gif') }})"></div>
                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="main-color section-title"  style="margin-right: 0">{{ __('main.admin-theme-settings') }}</h3>
                        <button class="btn btn-primary btn-collapse" type="button" data-toggle="collapse" data-target="#m" aria-expanded="true" aria-controls="m">
                            <i class="voyager-sort-desc"></i>
                        </button>
                    </div>

                    <div id="m" class="collapse in">

                        <div class="form-group">
                            <label>{{ __('main.image-dashboard') }}</label>
                            @php $bgImage = _setting('admin-bg_image'); @endphp
                            @if($bgImage)
                                <br>
                                <img id="admin-bg_image" style="width: 150px;" src="{{ asset("storage/$bgImage") }}" title="{{ __('main.image-dahboard') }}">
                                <br>
                            @endif
                            <input type="file" data-name="avatar" name="admin-bg_image" class="preview" data-preview-target="admin-bg_image">
                        </div>

                        <div class="form-group">
                            <label>{{ __('main.icon-dashboard') }}</label>
                            @php $icon_image = _setting('admin-icon_image'); @endphp
                            @if($icon_image)
                                <br>
                                <img id="admin-icon_image" style="width: 150px;" src="{{ asset("storage/$icon_image") }}" title="{{ __('main.icon-dashboard') }}">
                                <br>
                            @endif
                            <input type="file" name="admin-icon_image" class="preview" data-preview-target="admin-icon_image">
                        </div>

                        <div class="form-group">
                            <label>{{ __('main.dashboard-fond-color') }}</label>
                            <br>
                            <input class="form-control" type="color" name="admin-color" value="{{ _setting('admin-color') }}">
                        </div>

                        <button class="btn btn-primary" type="submit" style="margin-top: 15px; ">{{ __('main.buttons.save') }}</button>

                    </div>

                </div>
                </form>
            </div>
            <div class="col-md-6 pull-right">
                <form action="{{ route('voyager.settings.store') }}?fields=admin-title-ar,admin-description-ar,admin-title-en,admin-description-en" method="POST" class="ajax-form">
                    <div class="panel panel-body">
                        <div class="loading" style="background-image: url({{ asset('dashboard/images/loader.gif') }})"></div>
                        <div class="panel-heading" style="border-bottom:0;">
                            <h3 class="main-color section-title"  style="margin-right: 0">{{ __('main.dashboard-info') }}</h3>
                            <button class="btn btn-primary btn-collapse" type="button" data-toggle="collapse" data-target="#k" aria-expanded="true" aria-controls="k">
                                <i class="voyager-sort-desc"></i>
                            </button>
                        </div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs _nav-tabs" role="tablist" style="margin-bottom: 15px">
                            <li role="presentation" class="{{ app()->getLocale() == 'ar' ? 'active' : '' }}"><a href="#ar" aria-controls="home" role="tab" data-toggle="tab">{{ __('main.arabic') }}</a></li>
                            <li role="presentation" class="{{ app()->getLocale() == 'en' ? 'active' : '' }}"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">{{ __('main.english') }}</a></li>
                        </ul>

                        <div id="k" class="collapse in">

                            <div class="tab-content" style="margin: -20px;">

                                <div id="ar" role="tabpanel" class="tab-pane {{ app()->getLocale() == 'ar' ? 'active' : '' }}">
                                    <div class="form-group">
                                        <label>{{ __('main.dashboard-title') }}</label>
                                        <input class="form-control" type="text" name="admin-title-ar" value="{{ _setting('admin-title-ar') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.dashboard-desc') }}</label>
                                        <textarea rows="4" class="form-control" name="admin-description-ar">{{ _setting('admin-description-ar') }}</textarea>
                                    </div>
                                </div>

                                <div id="en" role="tabpanel" class="tab-pane {{ app()->getLocale() == 'en' ? 'active' : '' }}">
                                    <div class="form-group">
                                        <label>{{ __('main.dashboard-title') }}</label>
                                        <input class="form-control" type="text" name="admin-title-en" value="{{ _setting('admin-title-en') }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('main.dashboard-desc') }}</label>
                                        <textarea rows="4" class="form-control" name="admin-description-en">{{ _setting('admin-description-en') }}</textarea>
                                    </div>
                                </div>

                            </div>

                            <button class="btn btn-primary" type="submit" style="margin-top: 15px; ">{{ __('main.buttons.save') }}</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
@stop