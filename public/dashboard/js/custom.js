$(document).ready(function () {

    $('.mce-content-body').css('direction', 'rtl');

    /**
     * Sidebar Active
     */
    $('li.active a').each(function (e, element) {
        if ($(element).attr('href').indexOf(window.location.hostname + '/admin/users')) {
            if ($(element).attr('href') != window.location.href && !$($(element).parent('li').parent('ul')).hasClass('_nav-tabs')) {
                $(element).parent('li').removeClass('active');
            }
        }
        $(element).parent('li').parents('li').addClass('active');
    })


    /**
     * Handle Slug Input
     *
     */
    $('input[name=slug]').each(function (i, e) {
        originField = $(e).attr('data-slug-origin');
        if (originField) {
            originElement = $('input[name='+ originField +']');
            $(e).val(convertToSlug($(this).val()));
            originElement.keyup(function () {
                $(e).val(convertToSlug($(this).val()));
                return;
            });
            $(originElement.closest('form')).on('submit', function (ee) {
                $(e).val(convertToSlug($(originElement).val()));
            });
            originElement.blur(function() {
                $(e).val(convertToSlug($(this).val()));
                return false;
            });
        }
        $(e).keyup(function () {
            $(e).val(convertToSlug($(this).val()));
            return false;
        })
    });

    ajaxRequestSettings();

    preview();

});


/**
 * Generate SLug Function
 *
 * @param Text
 * @returns {string}
 */
function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/-+/g,'-')
        .replace(/[`~!@#$%^&*()_|+\=?;:'",.<>\{\}\[\]\\\/]/gi,'');
}



function ajaxRequestSettings()
{
    $('.ajax-form').submit(function (e) {
        e.preventDefault();
        var that = this;
        ajax(
            this,
            function() {
                $(that).find('.loading').show();
            },
            function (result) {
                if (result.success) {
                    toastr.success(result.success)
                }else {
                    toastr.error(result.error)
                }
                if (result.refresh) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000)
                }
            },
            function () {
                $(that).find('.loading').hide();
                $(that).trigger('focusout');
            }
        );

        return false;
    });
}


function ajax(form, beforeSend, success, done)
{
    var data = new FormData($(form)[0]);
    $.ajax({
        url: $(form).attr('action'),
        method: $(form).attr('method'),
        dataType: $(form).data('dataType') ? $(this).data('dataType') : 'json',
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: beforeSend,
        success: success
    }).done(done);
}


function removeImage(url, slug, image_name, beforeSend, success)
{
    $.ajax({
        url: url,
        method: "post",
        dataType: 'json',
        data: {image: image_name, slug: slug},
        beforeSend: beforeSend,
        success: success
    })
}


function preview()
{
    $('.preview').on('change', function () {
        preview_target = $(this).data('preview-target');
        document.getElementById(preview_target).src = window.URL.createObjectURL(this.files[0])
    });
}


function submitWhenChangeForm(that)
{
    $(that).closest('form').submit();
}