<?php

namespace App;

use App\Helpers\Roles;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use TCG\Voyager\Models\Role;
use Carbon\Carbon;
use TCG\Voyager\Contracts\User as UserContract;
use TCG\Voyager\Traits\VoyagerUser;

class User extends Authenticatable implements UserContract
{
    use Notifiable, VoyagerUser;

    protected $guarded = [];

    protected $hidden = ['password', 'remember_token'];

    public $additional_attributes = ['locale'];


    //============= Attributes ====================\\

    public function getAvatarAttribute($value)
    {
        return $value ?? config('voyager.user.default_avatar', 'users/default.png');
    }

    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function setSettingsAttribute($value)
    {
        $this->attributes['settings'] = $value->toJson();
    }

    public function getSettingsAttribute($value)
    {
        return collect(json_decode($value));
    }

    public function setLocaleAttribute($value)
    {
        $this->settings = $this->settings->merge(['locale' => $value]);
    }

    public function getLocaleAttribute()
    {
        return $this->settings->get('locale');
    }

    //============= #END# Attributes ================\\


    //============== Scopes ====================\\


    //============== #END Scope# ======================\\


    //============== Relations =========================\\



    //============== #END# Relations ====================\\


    //================ Roles ===========================\\

    //================ #END# Roles ========================\\

}
