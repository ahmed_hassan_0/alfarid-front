<?php
/**
 * Created by PhpStorm.
 * User: AHMED HASSAN
 */

namespace App\Http\View\Composers;
use App\Page;
use Illuminate\View\View;


class NavFooterComposer
{
    public function compose(View $view)
    {
        $dynamicPages = \App\Page::whereIn('id', [\App\Helpers\Pages::AboutUs, \App\Helpers\Pages::ContactUs])->get();
        $view->with(compact('dynamicPages'));
    }
}