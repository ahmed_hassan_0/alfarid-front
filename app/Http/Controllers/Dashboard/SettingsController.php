<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\Utilities;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    /**
     * Theme Settings
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function themeSettings(Request $request)
    {
        $locale = _arrayGet(auth()->user()->settings->toArray(), 'locale', 'ar');

        app()->setLocale($locale);

        return view('voyager::theme-settings.index');
    }

    /**
     * Save Admin Settings With Ajax
     *
     */
    public function adminSettings(Request $request)
    {
        abort_if(!$request->ajax(), 404);

        $fields = $request->fields;

        if (!$fields) return response()->json(['error' =>'fields is empty', 'refresh' => false]);

        $fieldsParse = explode(',', $fields);

        foreach ($fieldsParse as $field) {

            $value = null;

            //check if field is fill
            if ($request->filled($field)) {

                $value = $request->{$field};

            }elseif ($request->hasFile($field)) {
                //check if field is file
                $value = $request->file($field)->store('settings', 'public');
                if ($value) {
                    //remove old image
                    _clearAfterUpdateOrDelete(_setting($field));
                }
            }

            if ($value) {
                //set new value to key
                Utilities::updateSetting($field, ['value' => $value]);
            }
        }

        $refresh = in_array('refresh', $fieldsParse) ? true : false;

        return response()->json(['success' => 'تم تحديث الاعدادات بنجاح', 'refresh' => $refresh]);
    }
}
