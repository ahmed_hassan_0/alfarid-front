<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Testimonial;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class TestimonialController extends VoyagerBaseController
{
    public function store(Request $request)
    {
        $request->validate(['image' => 'required|image']);
        $nextOrder = (((int)Testimonial::orderBy('order', 'desc')->limit(1)->value('order')) + 1);
        $request->request->add(['order' => $nextOrder]);
        return parent::store($request);
    }
}
