<?php

namespace App\Http\Controllers\Theme;

use App\ArtisticFeature;
use App\Category;
use App\ContactUs;
use App\Event;
use App\Helpers\Limit;
use App\Helpers\OrderBY;
use App\Helpers\Pages;
use App\Helpers\Paginate;
use App\Http\Controllers\Controller;
use App\Offer;
use App\Page;
use App\Product;
use App\Testimonial;
use App\ThemeSlider;
use App\User;
use App\Work;
use App\Job;
use App\Gallary;
use App\JobsApplication;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request, $locale)
    {
        $data['pageTitle'] = __('main.home-title');
        $data['sliders'] = ThemeSlider::with('translations')->orderBy('order')->get();
        $data['products'] = Product::with('category')->with('translations')->where('display_home', 1)->latest()->limit(10)->get();
        $data['testimonials'] = Testimonial::orderBy('order')->get();
        $data['categories'] = Category::with('translations')->orderBy('order')->get();

        return view('theme.home', $data);
    }

    public function contactUsSubmit(Request $request)
    {
        $data = $request->validate(['name' => 'required', 'email' => 'required', 'phone' => 'required', 'subject' => 'required', 'message' => 'required']);
        $new_contact = ContactUs::forceCreate($data);
        abort_if(!$new_contact, 500);
        return redirect()->back()->with(['msg' => __('main.successful_contact_us'), 'type' => 'success']);
    }

    public function products(Request $request, $locale)
    {
        $data['pageTitle'] = __('main.products');
        $data['categories'] = Category::with('translations')->orderBy('order')->get();

        $query = Product::with('translations')->with('category');

        //filter by category if exists
        if ($request->category) {
            $category = Category::whereTranslation('slug', $request->category)->firstOrFail();
            $data['category'] = $category;
            $query->where('category_id', $category->id);
        }

        //search
        if ($request->search) {
            $query->whereTranslation('name', 'LIKE', '%' . $request->search . '%');
        }

        $data['products'] = $query->latest()->paginate(Paginate::Products);

        return view('theme.products', $data);
    }

    public function showProduct(Request $request, $locale, $slug)
    {
        $product = Product::with('category')->whereTranslation('slug', $slug)->firstOrFail();
        $data['product'] = $product;
        $data['pageTitle'] = $product->getTranslatedAttribute('name');
        $data['categories'] = Category::with('translations')->orderBy('order')->get();

        return view('theme.show-product', $data);
    }

    public function jobs(Request $request, $locale)
    {
        $items = Job::paginate(15);
        $pageTitle = __('main.jobs');
        return view('theme.jobs', compact('items', 'pageTitle'));
    }

    public function job($locale, $id)
    {
        $item = Job::findOrFail($id);
        return view('theme.job', compact('item'));
    }
    
    public function store(Request $request, $locale)
    {
        $validate = $request->validate([
            'job_id'=>'required|exists:jobs,id',
    
            'name'=>'required',
            
            'phone'=>'required',
            
            'info'=>'required',
            
            'cv'=>'required|file',
            
            ]);
    
            $create=JobsApplication::create($request->all());
    
    
            if(request()->has('cv')){
                        $create->update([
                        'cv' => request()->cv->store('uploads','public'),
                    ]);
                    }
    
    
    
             return redirect()->route('jobs.' . $locale, ['locale' => $locale])->with(['success' => __('main.success_job')]);
    }

    public function pages($locale, $page)
    {
        $page = Page::whereTranslation('slug', $page)->firstOrFail();
        $page_view = '';
        if (in_array($page->id, [Pages::AboutUs, Pages::ContactUs])) {
            $page_view = Pages::AboutUs == $page->id ? 'theme.pages.about-us' : 'theme.pages.contact-us';
        }
        $pageTitle = $page->getTranslatedAttribute('title', $locale);
        return view()->first(['theme.pages.' . $page->translate('en')->slug, $page_view, 'theme.pages.index'], compact('page', 'pageTitle'));
    }

    public function ProductPriceSubmit(Request $request)
    {
        app()->setLocale($request->localee);
        $request->validate(['product_id'=> 'required|exists:products,id','name' => 'required', 'company_name' => 'required', 'quantity' => 'required']);
        Offer::forceCreate([
            'product_id' => $request->product_id,
            'name' => $request->name,
            'company_name' => $request->company_name,
            'quantity' => $request->quantity,
            'email' => $request->email,
            'phone' => $request->phone
        ]);
        return back()->with(['msg' => __('main.successful_offer'), 'type' => 'success']);
    }

    public function gallary(Request $request, $locale)
    {
        $data['pageTitle'] = __('main.gallary');

        $query = Gallary::with('translations')->paginate(15);
        
        return view('theme.gallary',compact('query'));
    
    
    }
    




}
