<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class JobsApplication extends Model
{
    protected $table = 'jobs_applications';
    protected $guarded = ['title'];
}
