<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use TCG\Voyager\Traits\Translatable;


class Gallary extends Model
{

    
    use Translatable;

    
    protected $table = 'gallary';




    protected $translatable = ['image', 'info'];

}
