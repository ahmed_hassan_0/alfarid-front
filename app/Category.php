<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Category extends Model
{
    use Translatable;

    protected $translatable = ['name', 'slug'];


    //============= Relations ================\\

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    //============= #END# Relations ================\\
}
