<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Product extends Model
{
    use Translatable;

    protected $translatable = ['name', 'slug', 'description'];


    //============== Relations ================\\

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    //============== #END# Relations =============\\
}
