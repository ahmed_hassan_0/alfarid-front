<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Page extends Model
{
    use Translatable;

    protected $translatable = ['slug', 'title', 'body'];

    //=============== Scopes =============\\

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    //=============== #END# ==============\\

}
