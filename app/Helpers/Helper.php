<?php
/**
 * Created by PhpStorm.
 * User: AHMED HASSAN
 */



if (!function_exists('adminRows')) {
    function adminRows()
    {
        return [
            'name', 'email', 'phone', 'avatar', 'created_at'
        ];
    }
}


if (!function_exists('translateBreadcrumb')) {
    function translateBreadcrumb($segment, $default)
    {
        $_['admins'] = '> مديري الموقع';
        $_['common-questions'] = '> الاسئلة الشائعة';
        $_['categories'] = '> الاقسام';
        $_['roles'] = '> الصلاحيات';
        $_['pages'] = '> صحفات الموقع';
        $_['jobs'] = '> الوظائف';
        $_['products'] = '> المنتجات';
        $_['theme-sliders'] = '> سليدر';
        $_['artistic-features'] = '> مميزات فنوني';
        $_['theme-settings'] = '> اعدادات الموقع';
        $_['order'] = '> الترتيب';
        $_['contact-us'] = '> اتصل بنا';
        $_['testimonials'] = '> انطباع المتعاملين';
        $_['jobs-applications'] = '> متقدمي الوظائف';
        $_['edit'] = '> تعديل';
        $_['create'] = '> اضافة';
        $_['settings'] = '> الاعدادات';

        if (app()->getLocale() != 'ar') {
            return '> ' . ucfirst(str_replace('-', ' ', $segment));
        }


        return isset($_[$segment]) ? $_[$segment] : $default;
    }
}


if (!function_exists('isRoute')) {
    function isRoute($route_name) : bool
    {
        $uri_first_segment = @sscanf(urldecode(route($route_name)), url('') . '/%s')[0];
        return urldecode(url()->current()) == urldecode(route($route_name)) || request()->is(sprintf('*/%s', $uri_first_segment));
    }
}