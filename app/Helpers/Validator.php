<?php
/**
 * Created by PhpStorm.
 * User: AHMED HASSAN
 */

namespace App\Helpers;


class Validator
{

    public function _color($attribute, $value, $parameters, $validator)
    {
        return strpos($value, '#') !== false AND strlen($value) === 7;
    }

    public function _email($attribute, $value, $parameters, $validator)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    public function _phone($attribute, $value, $parameters, $validator)
    {
        return substr($value, 0, 2) == '01';
    }

    public function _password($attribute, $value, $parameters, $validator)
    {
        //Check Length!
        $length = isset($attribute[0]) ? $attribute[0] : 8;
        if (strlen($value) < $length) {
            return false;
        }

        //Contain At Least 1 Number!
        if (!preg_match('/[0-9]+/', $value)) {
            return false;
        }

        //Contain At Least 1 Capital Letter!
        if (!preg_match('/[A-Z]+/', $value)) {
            return false;
        }

        //Contain At Least 1 Lowercase Letter!
        if (!preg_match('/[a-z]+/', $value)) {
            return false;
        }

        return true;
    }

}