<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{



    //==================== Relations =====================\\

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function info()
    {
        return $this->hasOne(CountryInfo::class, 'iso', 'country_code');
    }

    //====================== END Relations ==================\\

}
