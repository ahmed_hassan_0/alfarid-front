<?php

namespace App\Providers;

use App\Actions\AddArtistToEventAction;
use App\Actions\ArtistWorksAction;
use App\FormFields\ListFormField;
use App\Http\View\Composers\NavFooterComposer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Voyager::addFormField(ListFormField::class);

        App::setLocale(_setting('default-language', 'en'));

        View::composer(
            ['theme.partials.nav', 'theme.partials.footer'],
            NavFooterComposer::class
        );
    }
}
